package com.example.myapplication;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.myapplication.pojo.Group;
import com.example.myapplication.pojo.Message;
import com.example.myapplication.pojo.Person;

import java.util.List;

public class ConstraintAndLinear extends Fragment {
    private List<Group> groupsList;

    private Spinner mSpinnerGroups;
    private Spinner mSpinnerPersons;
    private ImageButton mAddMessage;
    private EditText mInputMessageText;
    private int idLayout;

    public ConstraintAndLinear(List<Group> groupsList, int idLayout) {
        this.groupsList = groupsList;
        this.idLayout = idLayout;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(idLayout, null);

        initial(view);

        return view;
    }


    private void initial(View view) {
        mSpinnerGroups = view.findViewById(R.id.spinnerGroups);
        ArrayAdapter<Group> groupsAdapter = new ArrayAdapter<>(getActivity().getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, groupsList);
        mSpinnerGroups.setAdapter(groupsAdapter);

        mSpinnerPersons = view.findViewById(R.id.spinnerPersons);
        MessageAdapter adapter = new MessageAdapter(this.getContext());

        ListView messageView = view.findViewById(R.id.messageView);
        messageView.setAdapter(adapter);

        mInputMessageText = view.findViewById(R.id.inputMessageText);
        mAddMessage = view.findViewById(R.id.addMessage);

        setSpinnerGroupsListener(mSpinnerGroups, adapter);
        setSpinnerPersonsListener(mSpinnerPersons, adapter);
        setAddMessageListener(mAddMessage, adapter);

    }

    private void setAddMessageListener(ImageButton addMessage, final MessageAdapter adapter) {

        addMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String messageText = mInputMessageText.getText().toString();
                if (!messageText.equals("")) {
                    adapter.addMessage(new Message((Person) mSpinnerPersons.getSelectedItem(), messageText  ));

                    adapter.notifyDataSetChanged();
                    mInputMessageText.setText("");
                }
            }
        });
    }

    private void setSpinnerPersonsListener(Spinner spinnerPersons, final MessageAdapter adapter) {
        spinnerPersons.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                adapter.setActivePerson((Person) parent.getItemAtPosition(position));
                adapter.notifyDataSetChanged();
                TextView selectedText = (TextView) parent.getChildAt(0);
                if (selectedText != null) {
                    selectedText.setTextColor(Color.WHITE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void setSpinnerGroupsListener(Spinner spinnerGroups, final MessageAdapter adapter) {
        spinnerGroups.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ArrayAdapter<Person> personsAdapter = new ArrayAdapter<>(getActivity().getApplicationContext(),
                        android.R.layout.simple_spinner_dropdown_item, groupsList.get(position).getPersonsList());
                mSpinnerPersons.setAdapter(personsAdapter);
                adapter.setActiveGroup(groupsList.get(position));
                TextView selectedText = (TextView) parent.getChildAt(0);
                if (selectedText != null) {
                    selectedText.setTextColor(Color.WHITE);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


}
