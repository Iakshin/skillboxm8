package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;

import com.example.myapplication.pojo.Group;
import com.example.myapplication.pojo.Person;
import com.example.myapplication.twoButtonAndDescendant.TwoButton;
import com.example.myapplication.twoButtonAndDescendant.TwoButtonTwoText;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private FrameLayout mContainerFragment;

    private List<Group> groupsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initLists();

        initBottomNavigationView();
    }

    private void initLists() {
        groupsList = Arrays.asList(
                new Group("Банда Буччеллати",
                        Arrays.asList(new Person("Бруно Буччеллати"),
                                new Person("Джорно Джованна"),
                                new Person("Леоне Абаккио"),
                                new Person("Паннакотта Фуго"),
                                new Person("Наранча Гирга"),
                                new Person("Гвидо Миста"))),
                new Group("KonoSuba",
                        Arrays.asList(new Person("Кадзума"),
                                new Person("Аква"),
                                new Person("Мэгумин"),
                                new Person("Даркнесс"))),
                new Group("Команда 7",
                        Arrays.asList(new Person("Саске"),
                                new Person("Сакура"),
                                new Person("Наруто")))
        );

    }

    private void initBottomNavigationView() {
        BottomNavigationView bottomNavigation = findViewById(R.id.bottomNavigation);

        mContainerFragment = findViewById(R.id.containerFragment);

        bottomNavigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                //можно было бы синглтоны
                //последние два, специально не клонирую исходный объект, все осознанно
                switch (item.getItemId()) {
                    case R.id.twoButton:
                        loadFragment(new TwoButton(R.layout.two_button_fragment));
                        return true;
                    case R.id.twoButtonTwoText:
                        loadFragment(new TwoButtonTwoText(R.layout.two_button_two_text_fragment));
                        return true;
                    case R.id.constantLayout:
                        loadFragment(new ConstraintAndLinear(groupsList, R.layout.constraint_fragment));
                        return true;
                    case R.id.linearLayout:
                        loadFragment(new ConstraintAndLinear(groupsList, R.layout.linear_fragment));
                        return true;
                }
                return false;
            }
        });
    }

    private void loadFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(mContainerFragment.getId(), fragment);
        fragmentTransaction.commit();
    }

}
