package com.example.myapplication;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.myapplication.pojo.Group;
import com.example.myapplication.pojo.Message;
import com.example.myapplication.pojo.Person;

import java.util.List;

public class MessageAdapter extends BaseAdapter {

    private Person activePerson;
    private Group activeGroup;
    private List<Message> messageList;
    private Context context;

    public MessageAdapter(Context context) {
        this.context = context;
    }

    public void addMessage(Message message){
        messageList.add(message);
    }

    public void setActivePerson(Person activePerson) {
        this.activePerson = activePerson;
    }

    public void setActiveGroup(Group activeGroup) {
        this.activeGroup = activeGroup;
        messageList = activeGroup.getMessagesList();
    }

    @Override
    public int getCount() {
        if(messageList == null)
            return 0;
        return messageList.size();
    }

    @Override
    public Object getItem(int position) {
        return messageList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Log.d("opuy", String.valueOf(position));

        Message message = messageList.get(position);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        TextView messageText;
        TextView dataMessage;


        if(message.getPerson().equals(activePerson)){
            convertView = inflater.inflate(R.layout.message, null);

        } else {
            convertView = inflater.inflate(R.layout.message_from_others, null);
            TextView authorMessage = convertView.findViewById(R.id.authorMessage);
            authorMessage.setText(message.getPerson().getName());
        }

        messageText = convertView.findViewById(R.id.messageText);
        dataMessage = convertView.findViewById(R.id.dataMessage);

        messageText.setText(message.getTextMassage());
        dataMessage.setText(message.getDate());
        return convertView;
    }
}
