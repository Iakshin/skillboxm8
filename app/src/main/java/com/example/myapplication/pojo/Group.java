package com.example.myapplication.pojo;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

public class Group {
    private String name;
    private List<Message> messagesList;
    private List<Person> personsList;

    public Group() {
    }

    public Group(String name, List<Person> personsList) {
        this.name = name;
        this.personsList = personsList;
        messagesList = new ArrayList<>();
    }

    @NonNull
    @Override
    public String toString() {
        return name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;

    }

    public List<Message> getMessagesList() {
        return messagesList;
    }

    public void setMessagesList(List<Message> messagesList) {
        this.messagesList = messagesList;
    }

    public List<Person> getPersonsList() {
        return personsList;
    }

    public void addMassage(Message massage){
        messagesList.add(massage);
    }

}
