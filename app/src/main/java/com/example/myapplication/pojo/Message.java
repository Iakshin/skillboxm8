package com.example.myapplication.pojo;

import android.icu.util.LocaleData;

import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class Message {
    private Person person;
    private String textMassage;
    //I cry about LocalDate
    private Date date;
    private SimpleDateFormat formatData = new SimpleDateFormat("hh:mm");

    public Message(Person person, String textMassage, Date date) {
        this.person = person;
        this.textMassage = textMassage;
        this.date = date;
    }

    public Message(Person person, String textMassage) {
        this.person = person;
        this.textMassage = textMassage;
        date = new Date();
    }

    public Person getPerson() {
        return person;
    }

    public String getTextMassage() {
        return textMassage;
    }

    public String getDate() {
        return formatData.format(date);
    }
}
