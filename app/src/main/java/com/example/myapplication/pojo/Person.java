package com.example.myapplication.pojo;

public class Person {
    //коментарий для автора: попробовать record jdk14
    private String name;

    public Person(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

    public String getName() {
        return name;
    }

}
