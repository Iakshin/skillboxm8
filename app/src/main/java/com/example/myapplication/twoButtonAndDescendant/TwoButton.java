package com.example.myapplication.twoButtonAndDescendant;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.myapplication.R;

public class TwoButton extends Fragment {
    private int idLayout;
    private Button mMinusButton;
    private Button mPlusButton;
    private TextView mNumber;
    private int counter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(idLayout, null);

        initial(view);

        mMinusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                minusButton();
            }
        });

        mPlusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                plusButton();
            }
        });

        return view;
    }

    public TwoButton(int idLayout) {
        this.idLayout = idLayout;
    }

    protected void initial(View view){
        counter = 0;
        mMinusButton = view.findViewById(R.id.minusButton);
        mPlusButton = view.findViewById(R.id.plusButton);
        mNumber = view.findViewById(R.id.number);
    }

    protected void minusButton(){
        mNumber.setText(String.valueOf(--counter));
    }

    protected void plusButton(){
        mNumber.setText(String.valueOf(++counter));
    }
}
