package com.example.myapplication.twoButtonAndDescendant;

import android.view.View;
import android.widget.TextView;

import com.example.myapplication.R;

import java.util.Random;

public class TwoButtonTwoText extends TwoButton {
    private TextView mRandomNumber;

    public TwoButtonTwoText(int idLayout) {
        super(idLayout);
    }

    @Override
    protected void initial(View view) {
        super.initial(view);
        mRandomNumber = view.findViewById(R.id.randomNumber);
    }

    @Override
    protected void minusButton() {
        super.minusButton();
        mRandomNumber.setText(String.valueOf(new Random().nextInt()));
    }

    @Override
    protected void plusButton() {
        super.plusButton();
        mRandomNumber.setText(String.valueOf(new Random().nextInt()));
    }
}
